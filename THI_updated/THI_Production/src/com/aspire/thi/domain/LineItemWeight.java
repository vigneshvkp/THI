package com.aspire.thi.domain;

public class LineItemWeight {

	private Integer id;
	private Integer assesmentType;
	private Integer assesmentGroup;
	private String assessmentGroupName;
	private String lineItemText;
	private int weitage;
	
	public String getAssessmentGroupName() {
		return assessmentGroupName;
	}
	public void setAssessmentGroupName(String assessmentGroupName) {
		this.assessmentGroupName = assessmentGroupName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAssesmentType() {
		return assesmentType;
	}
	public void setAssesmentType(Integer assesmentType) {
		this.assesmentType = assesmentType;
	}
	public Integer getAssesmentGroup() {
		return assesmentGroup;
	}
	public void setAssesmentGroup(Integer assesmentGroup) {
		this.assesmentGroup = assesmentGroup;
	}
	public String getLineItemText() {
		return lineItemText;
	}
	public void setLineItemText(String lineItemText) {
		this.lineItemText = lineItemText;
	}
	public int getWeitage() {
		return weitage;
	}
	public void setWeitage(int weitage) {
		this.weitage = weitage;
	}

	
	

}
